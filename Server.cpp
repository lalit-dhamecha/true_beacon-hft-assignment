#include<iostream>
#include<cstring>
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <thread>
#include <winsock.h>
#include <ctime>
#include <sstream>
#include "Order.h"
#include <map>
#include <set>
bool cmpBuy(const Order* x, const Order* y) { return x->getOrderPrice() > y->getOrderPrice(); }
bool cmpSell(const Order* x, const Order* y) { return x->getOrderPrice() < y->getOrderPrice(); }

std::map<std::string, set<Order*, decltype(cmpBuy)* > > buyOrderMap;
std::map<std::string, set<Order*, decltype(cmpSell)* > > sellOrderMap;
std::map<std::string, set<Order*> > mapMktOrders;
std::set<int> dataPublishingClient;
bool isDataPublishStarted = false;
std::thread t1;
using namespace std;
#define PORT 9909
vector<std::string> files;
std::string directoryPath;
int delay;

struct sockaddr_in srv;
fd_set fr,fw,fe; //fr -readin,fw -writing,fe-exception
int nMaxFD;
int nSocket;
int nArrClinet[5];
void ProcessTheNewRequest();
void GetDataFeedBuffer(const std::string& line, std::string& buffer)
{
    vector <string> tokens;
    stringstream check1(line);
    string intermediate;
    while(getline(check1, intermediate, ','))
    {
        tokens.push_back(intermediate);
    }
    if(tokens.size() !=14)
    {
        cout << "Invalid Data . " << endl;
        return;
    }
    buffer += "DATAFEED|";
    buffer += "GlobalTicker="; buffer += tokens[0]; buffer += "|";
    buffer += "Date="; buffer += tokens[1]; buffer += "|";
    buffer += "LTP="; buffer += tokens[2]; buffer += "|";
    buffer += "Bid="; buffer += tokens[3]; buffer += "|";
    buffer += "BidSize="; buffer += tokens[4]; buffer += "|";
    buffer += "Ask="; buffer += tokens[5]; buffer += "|";
    buffer += "AskSize="; buffer += tokens[6]; buffer += "|";
    buffer += "LTQ="; buffer += tokens[7]; buffer += "|";
    buffer += "OpenInterest="; buffer += tokens[8]; buffer += "|";
    buffer += "Underlying="; buffer += tokens[9]; buffer += "|";
    buffer += "ExpiryDate="; buffer += tokens[10]; buffer += "|";
    buffer += "OptionType="; buffer += tokens[11]; buffer += "|";
    buffer += "StrikePrice="; buffer += tokens[12]; buffer += "|";
    buffer += "Ticker="; buffer += tokens[13]; 
    
}
void checkExecution(const std::string& buffer)
{
    vector <string> tokens;
    stringstream check1(buffer);
    string intermediate;
    while(getline(check1, intermediate, ','))
    {
        tokens.push_back(intermediate);
    }
    if(tokens.size() == 14)
    {
        auto it1 = mapMktOrders.find(tokens[13]);
        if(it1 != mapMktOrders.end())
        {
            for(auto it = it1->second.begin() ; it != it1->second.end(); it++ )
            {
                std::string buffer = "EXECUTION|";
                buffer += (*it)->getOrderId();buffer += "|";
                buffer += tokens[1];buffer += "|";
                buffer += std::to_string((*it)->getOrderSize());buffer += "|";
                if((*it)->getOrderSide() == "BUY")
                {
                    buffer += tokens[5];
                }
                else
                {
                        buffer += tokens[3];
                }
                
                int socket = (*it)->getClientId();
                cout << "Sending Execution msg " << buffer << " to client :" << socket << endl;
                int iResult = send(socket,buffer.c_str(),256,0);
                if (iResult == SOCKET_ERROR) {
                    wprintf(L"send failed with error: %d\n", WSAGetLastError());
                    
                }
                it1->second.erase(it);
                if(it1->second.empty())
                {
                    break;
                }
            }
        }

        auto it2 = buyOrderMap.find(tokens[13]);
        if(it2 != buyOrderMap.end())
        {
            for(auto it = it2->second.begin() ; it != it2->second.end(); it++)
            {
                if((*it)->getOrderPrice() == atof(tokens[5].c_str()))
                {
                    std::string buffer = "EXECUTION|";
                    buffer += (*it)->getOrderId();buffer += "|";
                    buffer += tokens[1];buffer += "|";
                    buffer += std::to_string((*it)->getOrderSize());buffer += "|";
                    buffer += tokens[5];
                    send((*it)->getClientId(),buffer.c_str(),256,0);
                    it2->second.erase(it);
                    if(it2->second.empty())
                    {
                        break;
                    }
                }
                
            }
        }

        auto it3 = sellOrderMap.find(tokens[13]);
        if(it3 != sellOrderMap.end())
        {
            for(auto it = it3->second.begin() ; it != it3->second.end(); it++)
            {
                if((*it)->getOrderPrice() == atof(tokens[3].c_str()))
                {
                    std::string buffer = "EXECUTION|";
                    buffer += (*it)->getOrderId();buffer += "|";
                    buffer += tokens[1];buffer += "|";
                    buffer += std::to_string((*it)->getOrderSize());buffer += "|";
                    buffer += tokens[3];
                    send((*it)->getClientId(),buffer.c_str(),256,0);
                    it3->second.erase(it);
                    if(it3->second.empty())
                    {
                        break;
                    }
                }
                
            }
        }
        
    }
    

}

void PublishData()
{
    if(!isDataPublishStarted)
    {
        return;
    }
    cout << "Data Publish Started. "  << endl;
    
    std::string filePath;
    for(auto it = files.begin(); it != files.end(); it++)
    {
        filePath = "";
        filePath += directoryPath;
        filePath += "\\" ;
        filePath += *it;
        cout << "File Name : " << filePath << endl;
        string line;
        ifstream inFile(filePath);
        getline (inFile, line); //skip first line
        while (getline (inFile, line)) {
            int tempDelay = delay;
            tempDelay *= CLOCKS_PER_SEC;
            clock_t now = clock();
            //cout << now <<  " : " << clock() << " : " << tempDelay << endl;
            while(clock() - now < tempDelay);
            std::string buffer;
            GetDataFeedBuffer(line,buffer);
            //cout << "Buffer : " << buffer << ", sizeof buffer : " << sizeof(buffer) << endl;
            for(auto it = dataPublishingClient.begin() ; it != dataPublishingClient.end(); it++)
            {
                send((*it),buffer.c_str(),256,0);
            }
            
            checkExecution(line);
        }
        inFile.close();
    }
    cout << "Data Publish complete. " << endl;
        

}
void ProcessNewMessage(int nClientSocket)
{
    cout << "Processing new message for client socket : " << nClientSocket << endl;
    char buff[256+1] = {0,};
    int nRet = recv(nClientSocket,buff,256,0);
    if(nRet < 0)
    {
        cout << "Something wrong happend..closing the connection for client socket " << nClientSocket << endl;
        closesocket(nClientSocket);
        for(int index = 0; index < 5; index++)
        {
            if(nArrClinet[index] == nClientSocket)
            {
                nArrClinet[index] = 0;
                break;
            }
        }
    }
    else
    {
        cout << "The message recived from the client is : "<< buff << endl;
        

        std::string buffer(buff);
        if(buffer == "PUBLISH_DATA")
        {
            dataPublishingClient.insert(nClientSocket);
            if(!isDataPublishStarted)
            {
                isDataPublishStarted = true;
                t1 = std::thread(PublishData);
                
            }
        }
        vector <std::string> tokens;
        stringstream check1(buffer);
        string intermediate;
        while(getline(check1, intermediate, '|'))
        {
            tokens.push_back(intermediate);
        }
        if(tokens.size() > 1)
        {
            if(tokens[0] == "ORDER")
            {
                if(tokens.size() == 7)
                {
                    std::string orderId = tokens[1];
                    std::string symbol = tokens[2];
                    std::string orderSide = tokens[3];
                    std::string orderSize = tokens[4];
                    std::string orderType = tokens[5];
                    std::string orderPrice = tokens[6];
                    Order* order = new Order(symbol,atoi(orderSize.c_str()),orderType,atof(orderPrice.c_str()),orderSide);
                    order->setOrderId(orderId);
                    order->setClientId(nClientSocket);
                    cout << "Received New Order : " << *order << endl;
                    if(order->getOrderType() == "MKT")
                    {
                        mapMktOrders[order->getSymbol()].insert(order);
                    }
                    else//Limit Order
                    {
                        if(orderSide == "BUY")
                        {
                            auto it = buyOrderMap.find(order->getSymbol());
                            if(it != buyOrderMap.end())
                            {
                                    buyOrderMap[order->getSymbol()].insert(order);
                            }
                            else
                            {
                                set<Order*, decltype(cmpBuy)* > s(cmpBuy);
                                s.insert(order);
                                buyOrderMap[order->getSymbol()] = s;
                            }
                            
                        }
                        else
                        {
                            auto it = sellOrderMap.find(order->getSymbol());
                            if(it != sellOrderMap.end())
                            {
                                    sellOrderMap[order->getSymbol()].insert(order);
                            }
                            else
                            {
                                set<Order*, decltype(cmpSell)* > s(cmpSell);
                                s.insert(order);
                                sellOrderMap[order->getSymbol()] = s;
                            }
                        }
                    }
                }
                
            }
        }
    }

}

void ProcessTheNewRequest()
{
    //check on which fd_set the request is avilable
    if(FD_ISSET(nSocket,&fe))
    {
            cout << "There is an exception " << endl;
    }
    if(FD_ISSET(nSocket,&fw))
    {
        cout << "There is ready to write something " << endl;
    }
    if(FD_ISSET(nSocket,&fr))
    {
        cout << "There is ready to read something " << endl;
        int nLen = sizeof(struct sockaddr);
        int nClientSocket = accept(nSocket,NULL, &nLen);
        if(nClientSocket > 0)
        {
            //accepted reques from client
            //Put it into the client fd_Set.
            int index;
            for(index = 0; index < 5; index++)
            {
                if(nArrClinet[index] == 0)
                {
                    nArrClinet[index] = nClientSocket;
                    send(nClientSocket,"Got the connection done successfully",37,0);
                    
                    break;
                }
            }
            if(index == 5)
            {
                cout << "No space for new connection "<< endl;
            }

        }

    }
    else
    {
        //to check if we have receive new request from existing clients
        for(int index = 0; index < 5; index++)
            {
                if(FD_ISSET(nArrClinet[index],&fr))
                {
                    //Got new message from client
                    //Just receive the new message
                    //just queue that for new worker of our server to full fill this request
                    ProcessNewMessage(nArrClinet[index]);
                }
            }
    }
}


int main(int argc, char *argv[]) 
{
    cout << "Argument count : " << argc << std::endl;
    
    int speed = 1;
    if(argc > 2)
    {
        directoryPath = argv[1];
        speed = std::atoi(argv[2]);
    }
    delay = 60/speed;
    cout << "Directory path : " << directoryPath << " , Delay : " << delay << "secs." << std::endl;

    //1.Get the files from datafeed directory
    DIR *dir; struct dirent *diread;
    
    if ((dir = opendir(directoryPath.c_str())) != nullptr) {
        while ((diread = readdir(dir)) != nullptr) {
            files.push_back(diread->d_name);
        }
        closedir (dir);
    } 
    std::sort(files.begin(), files.end());


    //2. Create server connection
    int nRet = 0;
    
    //initialize WSA variables is must for windows socket programming
    WSADATA ws;
    if(WSAStartup(MAKEWORD(2,2), &ws) < 0)
    {
        cout << "WSA failed to initialize" << endl;
    }
    else
    {
        cout << "WSA initialize" << endl;
    }
    //Initialize the socket
    nSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP); // 0 : means use underlying system protocol
    if(nSocket < 0)
    {
        cout << "Ths socket is not opened" << endl;
        WSACleanup();
        exit(EXIT_FAILURE);
    }
    else
    {
        cout<< "Socket openend successfully " << nSocket << endl;
    }
    //initialize the environment for sockaddr structure
    srv.sin_family = AF_INET;
    srv.sin_port = htons(PORT);
    srv.sin_addr.s_addr = INADDR_ANY; //address of local machine, if we want to provide our Ip addresss  inet_addr("127.0.0.1");
    memset(&(srv.sin_zero),0,8);

    // optval = 0 means blocking and !=0 means non blocking
    u_long optval = 0;
    nRet = ioctlsocket(nSocket,FIONBIO, &optval);
    if(nRet != 0)
    {
        cout << "ioctlsocket call failed " << endl;
    }
    else
    {
        cout << "ioctlsocket call passed " << endl;
    }

    //setsockopt
    int nOptVal = 0;
    int nOptLenght = sizeof(nOptVal);
    nRet = setsockopt(nSocket,SOL_SOCKET,SO_REUSEADDR,(const char*)& nOptVal,nOptLenght);
    if(nRet != 0)
    {
        cout << "setsockopt call failed " << endl;
        WSACleanup();
        exit(EXIT_FAILURE);
    }
    else
    {
        cout << "setsockopt call passed " << endl;
        
    }
    //Bind socket to a local port
    nRet = bind(nSocket,(sockaddr*)& srv, sizeof(sockaddr));
    if(nRet < 0)
    {
        cout << "Failed to bind to local port" << endl;
        WSACleanup();
        exit(EXIT_FAILURE);
    }
    else
    {
        cout << "Succesfully bind to local port " << endl;
    }
    
    // Listen the request from client (ques the request)
    nRet = listen(nSocket, 5);
    if(nRet < 0)
    {
        cout << "Failed to listen to local port" << endl;
        WSACleanup();
        exit(EXIT_FAILURE);
    }
    else
    {
        cout << "Succesfully listing to local port " << endl;
    }
    
    nMaxFD = nSocket;
    struct timeval tv; //timeout interval
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    
    
    while(1)
    {
        FD_ZERO(&fr);
        FD_ZERO(&fw);
        FD_ZERO(&fe);

        FD_SET(nSocket, &fr);
        FD_SET(nSocket, &fe);
        for(int index =0; index < 5;index++)
        {
            if(nArrClinet[index] != 0)
            {
                //to set the socket descriptors of new client connections
                FD_SET(nArrClinet[index], &fr);
                FD_SET(nArrClinet[index], &fe);
            }
        }
         //keep waiting for new request and proceed as per the request
        nRet = select(nMaxFD+1, &fr,&fw,&fe, &tv);
        if(nRet > 0)
        {
            //when someone connects or communicates with a message over a dedicated connection
            cout << "someone connects or communicates with a message over a dedicated connection" << endl; 
            //process the request
            ProcessTheNewRequest();
        }
        else if (nRet == 0)
        {
            //no connection or communication req made, 
            //none of the socket descriptors are ready
            //cout << "Nothing on port " << PORT << endl;
        }
        else
        {
            //It failed and your application should show some usefull message
            cout << "Select call failed " << endl;
            WSACleanup();
            exit(EXIT_FAILURE);
        }

    }
    t1.join();
    return 0;
}