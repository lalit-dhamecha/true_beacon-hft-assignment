#include<iostream>
#include <winsock.h>
#include <thread>
#include <map>
#include <sstream>
#include <vector>
using namespace std;


struct sockaddr_in srv;
fd_set fr,fw,fe; //fr -readin,fw -writing,fe-exception
int nClientSocket;
#define PORT 9909
char buff[512] ={0,};




void ReceiveDataFeed(int nClientSocket)
{
        while(1)
        {
            int nRet =recv(nClientSocket,buff,256,0);
            if(nRet < 0)
            {
                cout << "Something wrong happend..closing the connection for client socket " << nClientSocket << endl;
                closesocket(nClientSocket);
                break;
            }
            else
            {
                cout << buff << endl;
                
                
            }
            
        }
}

int main()
{
    int nRet = 0;
    //initialize WSA variables is must for windows socket programming
    WSADATA ws;
    if(WSAStartup(MAKEWORD(2,2), &ws) < 0)
    {
        cout << "WSA failed to initialize" << endl;
    }
    else
    {
        cout << "WSA initialize" << endl;
    }
    //Initialize the socket
    nClientSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP); // 0 : means use underlying system protocol
    if(nClientSocket < 0)
    {
        cout << "Ths socket is not opened" << endl;
    }
    else
    {
        cout<< "Socket openend successfully " << nClientSocket << endl;
    }
    //initialize the environment for sockaddr structure
    srv.sin_family = AF_INET;
    srv.sin_port = htons(PORT);
    srv.sin_addr.s_addr = inet_addr("127.0.0.1"); //address of local machine, if we want to provide our Ip addresss  inet_addr("127.0.0.1");
    memset(&(srv.sin_zero),0,8);

    //Connect socket to a local port
    nRet = connect(nClientSocket,(sockaddr*)& srv, sizeof(sockaddr));
    if(nRet < 0)
    {
        cout << "Failed to connected to server port" << endl;
        return 1;
    }
    else
    {
        cout << "Succesfully connected to server port " << endl;
        
        recv(nClientSocket,buff,512,0);
        cout << "Press any key to see the message received from server." << endl;
        getchar();
        cout << buff << endl;
        send(nClientSocket,"PUBLISH_DATA",32,0);
        std::thread t1(ReceiveDataFeed,nClientSocket);
        t1.join(); // join thread t1 to main thread
    
    }   

    return 0;
}