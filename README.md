# True_Beacon HFT Assignment

Create an exchange simulator for both market quotes and order execution.The simulator should be able to 

1. Replay historical tick by tick data from a file with a speed set by the user.
2. Have APIs to place order and retrieve order history

## Getting started

Files : 

1) Server.cpp : Acts as an Exchange, It recives the client connections for Publishing data feed and receiving Orders.
2) Client.cpp : Acts as client which connects to the Server for reciving market data feed only.
3) OrderRouting : API which connects to the Server for sending MKT/Limit orders and reciving executions.



Build Instructions :

Run following commands:
g++ Server.cpp  Order.cpp -lwsock32 -lws2_32 -o Server
g++ Client.cpp  -lwsock32 -lws2_32 -o Client
g++ OrderRouting.cpp  Order.cpp -lwsock32 -lws2_32 -o OrderRouting



Execution Instructions:
1) Server takes 2 arguments : 
a) Directory path for datafeed files.
b) Speed at which data is to be pulished (ex: if data is to be published at 2x speed, send the second argument as "2")

ex. : ./Server "C:\Users\Owner\Desktop\True_Beacon\Datafeed" 2

No arguments to be passed to Client and OrderRouting.


When the order routing binary is started it will show a user menu :
0.Exit
1.Add New Order:
2.View Order History :


To add the new order press 1:
Sample new order message to be provided on Standard input (cin).

<Ticker> <OrderSide> <OrderSize> <OrderType> <OrderPrice>
BANKNIFTY28MAY2024500CE BUY 10 LIM 11.95
BANKNIFTY28MAY2024500CE SELL 10 LIM 11.95
BANKNIFTY28MAY2024500CE SELL 10 MKT 0



to view the order status press 2 :
Symbol : BANKNIFTY28MAY2024500CE OrderId : ABC0001 OrderQty : 10 OrderType : MKT OrderPrice : 0 OrderSide : BUY OrderState : COMPLETED OrderTime : 2020-05-15T09:22:41Z ExecQty : 10 ExecPrice : 11.25 ClientId : 0

Symbol : BANKNIFTY28MAY2024500CE OrderId : ABC0002 OrderQty : 10 OrderType : LIM OrderPrice : 11.95 OrderSide : BUY OrderState : COMPLETED OrderTime : 2020-05-15T09:18:12Z ExecQty : 10 ExecPrice : 11.95 ClientId : 0

Symbol : BANKNIFTY28MAY2024500CE OrderId : ABC0003 OrderQty : 10 OrderType : LIM OrderPrice : 13.55 OrderSide : SELL OrderState : NEW OrderTime :  ExecQty : 0 ExecPrice : 0 ClientId : 0

Symbol : BANKNIFTY28MAY2024500CE OrderId : ABC0004 OrderQty : 10 OrderType : LIM OrderPrice : 11.95 OrderSide : SELL OrderState : NEW OrderTime :  ExecQty : 0 ExecPrice : 0 ClientId : 0



Note :Both the datafeed and Order status messages will be shown on Standard output (cout).
