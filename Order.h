#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include <cstring>
static int counter = 1;
using namespace std;
static void GetOrderId(std::string& orderId /*output*/)
{
    orderId += "ABC000";
    orderId += std::to_string(counter++);
}
class Order
{
private:
    std::string orderId;
    std::string symbol;
    int orderSize;
    double price;
    std::string orderType;
    std::string orderTime;
    std::string orderState;
    std::string orderSide;
    int execQty;
    double execPrice;
    int clientId;
public:
    Order(std::string symbol,int orderSize, std::string orderType,double price, std::string orderSide);
    ~Order();
    void UpdateOrderState();
    void setExecutionReport(std::string orderTime,int execQty, double price);
    std::string getOrderId() const { return  orderId; }
    std::string getSymbol() const { return  symbol; }
    std::string getOrderSide() const { return  orderSide; }
    std::string getOrderType() const { return  orderType; }
    int getOrderSize() const { return  orderSize; }
    double getOrderPrice() const { return  price; }
    void setOrderId(const std::string& orderId) { this->orderId = orderId; }
    void setClientId(int id ) { this->clientId = id;}
    int getClientId() const {return clientId;}
    friend ostream & operator << (ostream &out, const Order &ord);
};

#endif