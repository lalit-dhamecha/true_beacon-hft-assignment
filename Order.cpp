#include "Order.h"
using namespace std;

Order::Order(std::string symbol,int orderSize, std::string orderType,double price, std::string orderSide)
{
    this->symbol = symbol;
    this->orderSize = orderSize;
    this->orderType = orderType;
    if(this->orderType == "MKT")
    {
        this->price = 0;
    }
    else
    {
        this->price = price;
    }
    
    this->execPrice = 0;
    this->execQty = 0;
    this->orderTime = "";
    this->orderState = "NEW";
    this->orderSide = orderSide;
    //GetOrderId(this->orderId);
}

Order::~Order()
{
}
void Order::UpdateOrderState()
{
    if(this->execQty == 0)
    {
        this->orderState = "NEW";
    }
    else if(this->execQty < this->orderSize)
    {
        this->orderState = "PART";
    }
    else if(this->execQty == this->orderSize)
    {
        this->orderState = "COMPLETED";
    }
}
void Order::setExecutionReport(std::string orderTime,int execQty, double price)
{
    this->orderTime = orderTime;
    this->execQty = execQty;
    this->execPrice = price;
    UpdateOrderState();
}
ostream & operator << (ostream &out, const Order &ord)
{
    out << "Symbol : " << ord.symbol;
    out << " OrderId : " << ord.orderId;
    out << " OrderQty : " << ord.orderSize;
    out << " OrderType : " << ord.orderType;
    out << " OrderPrice : " << ord.price;
    out << " OrderSide : " << ord.orderSide;
    out << " OrderState : " << ord.orderState;
    out << " OrderTime : " << ord.orderTime;
    out << " ExecQty : " << ord.execQty;
    out << " ExecPrice : " << ord.execPrice;
    out << " ClientId : " << ord.clientId;
    out << endl;
    return out;
}
 