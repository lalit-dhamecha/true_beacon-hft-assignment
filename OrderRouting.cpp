#include<iostream>
#include <winsock.h>
#include <thread>
#include "Order.h"
#include <map>
#include <sstream>
#include <vector>
using namespace std;

std::map<std::string, Order* > ordersMap;
struct sockaddr_in srv;
fd_set fr,fw,fe; //fr -readin,fw -writing,fe-exception
int nClientSocket;
#define PORT 9909
char buff[512] ={0,};

void PrintOrderHistory()
{
    for(auto it = ordersMap.begin(); it != ordersMap.end(); it++)
    {
        cout << *(it->second) << endl;
    }
}

void AddNewOrder(int nClientSocket)
{
    int orderSize;
    double orderPrice;
    std::string symbol,orderSide, orderType;
    cout << "Enter Order details " << endl;
    cout << "<Symbol> <OrderSide> <OrderSize> <OrderType> <OrderPrice>" << endl;
    cin >> symbol >> orderSide >> orderSize >> orderType >> orderPrice;
    Order* order = new Order(symbol,orderSize,orderType,orderPrice,orderSide);
    std::string orderId;
    GetOrderId(orderId); //Generate new orderId
    order->setOrderId(orderId);
    ordersMap.insert({order->getOrderId(), order});

    std::string buffer = "ORDER|";
    buffer += order->getOrderId();buffer += "|";
    buffer += order->getSymbol();buffer += "|";
    buffer += order->getOrderSide();buffer += "|";
    buffer += std::to_string(order->getOrderSize());buffer += "|";
    buffer += order->getOrderType();buffer += "|";
    buffer += std::to_string(order->getOrderPrice());

    cout << "Sending Order : " << buffer << endl;
    send(nClientSocket,buffer.c_str(),256,0);
}
void ReceiveOrderExecution(int nClientSocket)
{
        while(1)
        {
            
            int nRet =recv(nClientSocket,buff,256,0);
            
            if(nRet < 0)
            {
                cout << "Something wrong happend..closing the connection for client socket " << nClientSocket << endl;
                closesocket(nClientSocket);
                break;
            }
            else
            {
                cout << endl;
                cout << buff << endl;
                std::string buffer(buff);
                vector <std::string> tokens;
                stringstream check1(buffer);
                string intermediate;
                while(getline(check1, intermediate, '|'))
                {
                    tokens.push_back(intermediate);
                }
                if(tokens.size() > 1)
                {
                    if(tokens[0] == "EXECUTION")
                    {
                        if(tokens.size() == 5)
                        {
                            std::string orderId = tokens[1];
                            std::string orderTime = tokens[2];
                            std::string execSize = tokens[3];
                            std::string execPrice = tokens[4];
                            auto it = ordersMap.find(orderId);
                            if(it != ordersMap.end())
                            {
                                cout << "Received Execution on order " << orderId << " ExecSize : " << execSize << " ExecPrice : " << execPrice << " ExecTime : " << orderTime << endl;
                                it->second->setExecutionReport(orderTime, atoi(execSize.c_str()), atof(execPrice.c_str()));
                            }
                            else
                            {
                                cout << "No Data Found For order : " << orderId << endl;
                            }
                            
                        }
                        
                    }
                }
            }
        }
}
int user_action( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Add New Order: "<<endl;
	cout<<"2.View Order History :"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main()
{
    int nRet = 0;
    //initialize WSA variables is must for windows socket programming
    WSADATA ws;
    if(WSAStartup(MAKEWORD(2,2), &ws) < 0)
    {
        cout << "WSA failed to initialize" << endl;
    }
    else
    {
        cout << "WSA initialize" << endl;
    }
    //Initialize the socket
    nClientSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP); // 0 : means use underlying system protocol
    if(nClientSocket < 0)
    {
        cout << "Ths socket is not opened" << endl;
    }
    else
    {
        cout<< "Socket openend successfully " << nClientSocket << endl;
    }
    //initialize the environment for sockaddr structure
    srv.sin_family = AF_INET;
    srv.sin_port = htons(PORT);
    srv.sin_addr.s_addr = inet_addr("127.0.0.1"); //address of local machine, if we want to provide our Ip addresss  inet_addr("127.0.0.1");
    memset(&(srv.sin_zero),0,8);

    //Connect socket to a local port
    nRet = connect(nClientSocket,(sockaddr*)& srv, sizeof(sockaddr));
    if(nRet < 0)
    {
        cout << "Failed to connected to server port" << endl;
        return 1;
    }
    else
    {
        cout << "Succesfully connected to server port " << endl;
        
        recv(nClientSocket,buff,512,0);
        cout << "Press any key to see the message received from server." << endl;
        getchar();
        cout << buff << endl;
        
        std::thread t1(ReceiveOrderExecution,nClientSocket);
        
        int choice;
        while( ( choice = ::user_action( ) ) != 0 )
        {
            switch( choice )
            {
            case 1:
                AddNewOrder(nClientSocket);
                break;
            case 2:
                PrintOrderHistory();
                break;
            }
            
        }
        t1.join(); // join thread t1 to main thread
    
    }   

    return 0;
}